<?php

declare(strict_types=1);

namespace zeageorge\MySQLDataSource_7234;

use Exception;

/**
 * Description of MySQLDataSourceException
 *
 * @author George Zeakis <zeageorge@gmail.com>
 */
class MySQLDataSourceException extends Exception {
}
