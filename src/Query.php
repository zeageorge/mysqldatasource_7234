<?php

declare(strict_types=1);

namespace zeageorge\MySQLDataSource_7234;

use PDO, JsonSerializable;

/**
 * Description of Query
 *
 * @author George Zeakis <zeageorge@gmail.com>
 * @see https://www.php.net/manual/en/pdostatement.fetchall.php
 */
class Query implements JsonSerializable {
  /** @var string */
  protected $query;

  /** @var array */
  protected $bind_params;

  /** @var int */
  protected $fetch_style;

  /** @var mixed */
  protected $fetch_argument;

  /** @var array */
  protected $constructor_args;

  /**
   * To get just anonymous objects use: $fetch_style = PDO::FETCH_OBJ
   *
   * To get an array of custom objects use
   * $fetch_style = PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE
   * $fetch_argument = 'MyClass'
   *
   * @param string $query
   * @param array $bind_params
   * @param int $fetch_style
   * @param mixed $fetch_argument
   * @param array $constructor_args
   * @see https://www.php.net/manual/en/pdostatement.fetchall.php
   */
  public function __construct(array $params = []) {
    foreach ($params + [
      'query' => '',
      'bind_params' => [],
      'fetch_style' => PDO::FETCH_OBJ,
      'fetch_argument' => null,
      'constructor_args' => [],
    ] as $key => $value) {
      $this->$key = $value;
    }
  }

  /**
   *
   * @return string
   */
  public function getQuery(): string {
    return $this->query;
  }

  /**
   *
   * @return array
   */
  public function getBindParams(): array {
    return $this->bind_params;
  }

  /**
   *
   * @return int
   */
  public function getFetchStyle(): int {
    return $this->fetch_style;
  }

  /**
   *
   * @return mixed
   */
  public function getFetchArgument() {
    return $this->fetch_argument;
  }

  /**
   *
   * @return array
   */
  public function getConstructorArgs(): array {
    return $this->constructor_args;
  }

  /**
   *
   * @param string $query
   * @return self
   */
  public function setQuery(string $query): self {
    $this->query = $query;
    return $this;
  }

  /**
   *
   * @param string $name
   * @param mixed $value
   * @return self
   */
  public function addBindParams(string $name, $value): self {
    $this->bind_params[$name] = $value;
    return $this;
  }

  /**
   *
   * @param string $name
   * @return self
   */
  public function setBindParams(array $bind_params): self {
    $this->bind_params = $bind_params;
    return $this;
  }

  /**
   *
   * @return self
   */
  public function clearBindParams(): self {
    $this->bind_params = [];
    return $this;
  }

  /**
   *
   * @param int $fetch_style
   * @return self
   */
  public function setFetchStyle(int $fetch_style): self {
    $this->fetch_style = $fetch_style;
    return $this;
  }

  /**
   *
   * @param mixed $fetch_argument
   * @return self
   */
  public function setFetchArgument($fetch_argument): self {
    $this->fetch_argument = $fetch_argument;
    return $this;
  }

  /**
   *
   * @param array $constructor_args
   * @return self
   */
  public function setConstructorArgs(array $constructor_args): self {
    $this->constructor_args = $constructor_args;
    return $this;
  }

  /**
   *
   * @return mixed
   */
  public function jsonSerialize(): mixed {
    return (object) [
      'query' => $this->query,
      'bind_params' => $this->bind_params,
      'fetch_style' => $this->fetch_style,
      'fetch_argument' => $this->fetch_argument,
      'constructor_args' => $this->constructor_args,
    ];
  }
}
