<?php

declare(strict_types=1);

namespace zeageorge\MySQLDataSource_7234;

use Throwable, JsonSerializable;
use PDO, PDOException, PDOStatement;
use function
  count,
  gettype,
  strpos,
  strtolower,
  implode,
  array_map,
  rtrim,
  is_int,
  is_float,
  addcslashes,
  str_replace,
  join,
  explode,
  array_keys;

/**
 * Description of MySQLDataSource
 *
 * @author George Zeakis <zeageorge@gmail.com>
 */
class MySQLDataSource implements JsonSerializable {
  /** @var PDO|null */
  protected $pdo = null;

  /** @var string */
  protected $host = 'localhost';

  /** @var int */
  protected $port = 3306;

  /** @var string */
  protected $db_name = '';

  /** @var string */
  protected $username = '';

  /** @var string */
  protected $password = '';

  /** @var string */
  protected $table_prefix_placeholder = '#__';

  /** @var string */
  protected $table_prefix = '';

  /** @var string */
  protected $charset = 'utf8';

  /** @var string */
  protected $quote_character = "`";

  /** @var array */
  protected $connection_options = [
    PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'UTF8'"
  ];

  /** @var array */
  protected $connection_attributes = [
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
    PDO::ATTR_EMULATE_PREPARES => false,
  ];

  /**
   *
   * @return self
   */
  public function open(): self {
    if ($this->pdo != null) { // connection already opened
      return $this;
    }

    $dsn = "mysql:host={$this->host};port={$this->port};dbname={$this->db_name}";

    // because of:
    // Warning: If your application does not catch the exception thrown from the PDO constructor,
    // the default action taken by the zend engine is to terminate the script and display a back trace.
    // This back trace will likely reveal the full database connection details, including the username and password.
    // It is your responsibility to catch this exception, either explicitly (via a catch statement) or implicitly
    // via set_exception_handler().
    // @see https://www.php.net/manual/en/pdo.connections.php
    try {
      $this->pdo = new PDO($dsn, $this->username, $this->password, $this->connection_options);

      foreach ($this->connection_attributes as $key => $value) {
        $this->pdo->setAttribute($key, $value);
      }

      !empty($this->charset) && $this->pdo->exec("SET CHARACTER SET " . $this->pdo->quote($this->charset));
    } catch (Throwable $th) {
      throw new MySQLDataSourceException($th->getMessage(), 0, $th);
    }

    return $this;
  }

  /**
   *
   * @return self
   */
  public function close(): self {
    $this->pdo = null;

    return $this;
  }

  /**
   * execQuery
   *
   * @param Query $query
   * @return array type of array items depends on $fetch_style of $query.
   *   if the query is an 'insert...' then [the id of the inserted row] is returned
   *   if the query is an 'update...' or 'delete...' then [the number of affected rows] is returned
   * @throws MySQLDataSourceException
   * @throws PDOException
   * @see https://www.php.net/manual/en/pdostatement.fetchall.php
   */
  public function execQuery(Query $query): array {
    /** @var string */
    $query_str = $query->getQuery();

    /** @var PDOStatement */
    $sth = $this->pdo->prepare($this->getPrefixedTableName($query_str));

    foreach ($query->getBindParams() as $key => $value) {
      $sth->bindValue("{$key}", $value, $this->getPdoType(gettype($value)));
    }

    // $sth->execute()
    // return bool — TRUE on success or FALSE on failure.
    // throws \PDOException — On error if PDO::ERRMODE_EXCEPTION option is true.
    if (false === $sth->execute()) {
      $sth = null;

      throw new MySQLDataSourceException("Execution of prepared statement failed");
    }

    if (strpos(strtolower($query_str), 'insert') === 0) {
      /** @var string */
      $first_insert_id = $this->pdo->lastInsertId();

      $sth = null;

      return [$first_insert_id];
    } elseif (strpos(strtolower($query_str), 'update') === 0 || strpos(strtolower($query_str), 'delete') === 0) {
      /** @var int */
      $affected_rows = $sth->rowCount();

      $sth = null;

      return [$affected_rows];
    }

    // All other queries return results(?), so we can fetch and return them:
    if (false === $result_set = $sth->fetchAll(...$this->getFetchArgs($query))) {
      $sth = null;

      throw new MySQLDataSourceException("Couldn't fetch results");
    }

    $sth = null;

    return $result_set;
  }

  /**
   * Helper method to deal with https://bugs.php.net/bug.php?id=75387
   *
   * @param Query $query
   * @return array
   */
  protected function getFetchArgs(Query $query): array {
    // there always should be a fetch style !
    $args = [$query->getFetchStyle()];

    if (null != $fetch_arg = $query->getFetchArgument()) {
      $args[] = $fetch_arg;
    }

    if ((null != $constr_args = $query->getConstructorArgs()) && count($constr_args)) {
      $args[] = $constr_args;
    }

    return $args;
  }

  /**
   * insert one record into a table
   *
   * @param string $table_name
   * @param array $record An associative array, eg. ['column1' => 'value','column2' => 'value', 'ColumnN' => 'value']
   * @return string Returns the id of the inserted row
   * @throws PDOException
   * @throws MySQLDataSourceException
   */
  public function insert(string $table_name, array $record): string {
    if (empty($table_name)) {
      throw new MySQLDataSourceException("Table name can't be empty", 0);
    }

    if (count($record) < 1) {
      throw new MySQLDataSourceException("Record array can't be empty", 1);
    }

    /** @var string */
    $column_names = implode(',', array_map([$this, 'quoteColumnName'], array_keys($record)));

    /** @var string */
    $column_values_placeholders = ':' . implode(', :', array_keys($record));

    /** @var string */
    $sql_query = "INSERT INTO {$this->quoteTableName($table_name)} ({$column_names}) VALUES ({$column_values_placeholders});";

    /** @var PDOStatement */
    $sth = $this->pdo->prepare($this->getPrefixedTableName($sql_query));

    foreach ($record as $column_value_placeholder => $column_value) {
      $sth->bindValue(":{$column_value_placeholder}", $column_value, $this->getPdoType(gettype($column_value)));
    }

    // $sth->execute()
    // return bool — TRUE on success or FALSE on failure.
    // throws \PDOException — On error if PDO::ERRMODE_EXCEPTION option is true.
    if (false === $sth->execute()) {
      $sth = null;

      throw new MySQLDataSourceException("Execution of prepared statement failed", 3);
    }

    /** @var string */
    $first_insert_id = $this->pdo->lastInsertId();

    $sth = null;

    return $first_insert_id;
  }

  /**
   * Insert records into a table
   * All records must have the same column order
   * Use of transactions. if any insertion fails then roll back all.
   *
   * @param string $table_name name of the table to insert into
   * @param array $column_names An array of column names
   * @param array $values An array of records. All record values must have the same column order.
   *  eg. [
   *   ['v1', 1, 'test1'],
   *   ['v2', 2, 'test2'],
   *   ['v3', 3, 'test3'],
   *   ['v4', 4, 'test4'],
   * ]
   * @return string Returns the autogenerated id of the first inserted row
   * @throws PDOException
   * @throws MySQLDataSourceException
   * @see http://stackoverflow.com/questions/1176352/pdo-prepared-inserts-multiple-rows-in-single-query
   * @see http://stackoverflow.com/questions/19680494/insert-multiple-rows-with-pdo-prepared-statements
   * @see http://stackoverflow.com/questions/10060721/pdo-mysql-insert-multiple-rows-in-one-query
   * @see http://dev.mysql.com/doc/refman/5.7/en/load-data.html
   */
  public function insertMultiple(string $table_name, array $column_names, array $values): string {
    if (empty($table_name)) {
      throw new MySQLDataSourceException("Table name can't be empty", 0);
    }

    if (count($column_names) < 1) {
      throw new MySQLDataSourceException("Column names array can't be empty", 1);
    }

    if (count($values) < 1) {
      throw new MySQLDataSourceException("Values array can't be empty", 2);
    }

    /** @var PDOStatement */
    $sth = null;

    try {
      /** @var string */
      $column_names_str = implode(',', array_map([$this, 'quoteColumnName'], $column_names));

      /** @var string */
      $sql_query = "INSERT INTO {$this->quoteTableName($table_name)} ({$column_names_str}) VALUES ";

      /** @var string */
      $values_str = '';

      /** @var int */
      $i = 0;

      foreach ($values as $rec_values) {
        $values_str .= "(";
        foreach ($rec_values as $value) {
          $values_str .= ":v{$i},";
          $i++;
        }
        $values_str = rtrim($values_str, ',') . '),';
      }
      $sql_query .= rtrim($values_str, ',') . ";";

      $sth = $this->pdo->prepare($this->getPrefixedTableName($sql_query));

      $i = 0;
      foreach ($values as $rec_values) {
        foreach ($rec_values as $value) {
          $sth->bindValue(':v' . $i++, $value, $this->getPdoType(gettype($value)));
        }
      }

      // $this->pdo->beginTransaction()
      // Returns TRUE on success or FALSE on failure.
      // Throws a PDOException if there is already a transaction started or the driver does not support transactions.
      // Note: An exception is raised even when the PDO::ATTR_ERRMODE attribute is not PDO::ERRMODE_EXCEPTION.
      if (false === $this->pdo->beginTransaction()) {
        $sth = null;

        throw new MySQLDataSourceException("Can't begin transaction", 3);
      }

      // $sth->execute()
      // return bool — TRUE on success or FALSE on failure.
      // throws \PDOException — On error if PDO::ERRMODE_EXCEPTION option is true.
      if (false === $sth->execute()) {
        $sth = null;

        throw new MySQLDataSourceException("Execution of prepared statement failed", 4);
      }

      /** @var string */
      $first_insert_id = $this->pdo->lastInsertId();

      // $this->pdo->commit()
      // Returns TRUE on success or FALSE on failure.
      // Throws a PDOException if there is no active transaction.
      // Note: An exception is raised even when the PDO::ATTR_ERRMODE attribute is not PDO::ERRMODE_EXCEPTION.
      if (false === $this->pdo->commit()) {
        $sth = null;

        throw new MySQLDataSourceException("Can't commit transaction", 5);
      }

      $sth = null;

      return $first_insert_id;
    } catch (Throwable $th) {
      // $this->pdo->rollback() Throws a PDOException if there is no active transaction.
      // Note: An exception is raised even when the PDO::ATTR_ERRMODE attribute is not PDO::ERRMODE_EXCEPTION.
      // @see https://www.php.net/manual/en/pdo.rollback.php
      if ($this->pdo->inTransaction() && !$this->pdo->rollback()) {
        $sth = null;

        throw new MySQLDataSourceException("Can't rollback", 6, $th);
      }

      $sth = null;

      throw $th;
    }
  }

  /**
   * Update
   *
   * @param string $table_name
   * @param array $data_to_update An associative array, eg. ['Column1' => 'value', 'Column2' => 'value', 'ColumnN' => 'value']
   * @param string $where the WHERE query part AFTER the WHERE keyword
   * @param array $bind_params for the where part. eg. [':id1' => 'someValue1', ':id2' => 'someValue2', ':idN' => 'someValueN']
   * @return int Return the number of affected rows
   * @throws PDOException
   * @throws MySQLDataSourceException
   */
  public function update(string $table_name, array $data_to_update, string $where = null, array $bind_params = []): int {
    if (empty($table_name)) {
      throw new MySQLDataSourceException("Table name can't be empty", 0);
    }

    if (count($data_to_update) < 1) {
      throw new MySQLDataSourceException("data_to_update array can't be empty", 1);
    }

    /** @var string */
    $field_details = '';

    foreach ($data_to_update as $key => $value) {
      $field_details .= "{$this->quoteColumnName($key)}=:{$key},";
    }
    $field_details = rtrim($field_details, ',');

    /** @var string */
    $sql_query = "UPDATE {$this->quoteTableName($table_name)} SET {$field_details}" . ($where != null ? " WHERE {$where};" : ";");

    /** @var PDOStatement */
    $sth = $this->pdo->prepare($this->getPrefixedTableName($sql_query));

    foreach ($data_to_update as $key => $value) {
      $sth->bindValue(":{$key}", $value, $this->getPdoType(gettype($value)));
    }

    foreach ($bind_params as $key => $value) {
      $sth->bindValue("{$key}", $value, $this->getPdoType(gettype($value)));
    }

    // $sth->execute()
    // return bool — TRUE on success or FALSE on failure.
    // throws \PDOException — On error if PDO::ERRMODE_EXCEPTION option is true.
    if (false === $sth->execute()) {
      $sth = null;

      throw new MySQLDataSourceException("Execution of prepared statement failed", 2);
    }

    /** @var int */
    $affected_rows = $sth->rowCount();

    $sth = null;

    return $affected_rows;
  }

  /**
   * Delete
   *
   * @param string $table_name
   * @param string $where the WHERE query part AFTER the WHERE keyword
   * @param array $bind_params for the where part. eg. [':id1' => 'someValue1', ':id2' => 'someValue2', ':idN' => 'someValueN']
   * @return int Return the number of affected rows
   * @throws PDOException
   * @throws MySQLDataSourceException
   */
  public function delete(string $table_name, string $where = null, array $bind_params = []): int {
    if (empty($table_name)) {
      throw new MySQLDataSourceException("Table name can't be empty", 0);
    }

    /** @var string */
    $sql_query = "DELETE FROM {$this->quoteTableName($table_name)}" . ($where != null ? " WHERE {$where};" : ";");

    /** @var PDOStatement */
    $sth = $this->pdo->prepare($this->getPrefixedTableName($sql_query));

    foreach ($bind_params as $key => $value) {
      $sth->bindValue("{$key}", $value, $this->getPdoType(gettype($value)));
    }

    // $sth->execute()
    // return bool — TRUE on success or FALSE on failure.
    // throws \PDOException — On error if PDO::ERRMODE_EXCEPTION option is true.
    if (false === $sth->execute()) {
      $sth = null;

      throw new MySQLDataSourceException("Execution of prepared statement failed", 2);
    }

    /** @var int */
    $affected_rows = $sth->rowCount();

    $sth = null;

    return $affected_rows;
  }

  /**
   * Quotes a string value for use in a query.
   *
   * @param string $str string to be quoted
   * @return string the properly quoted string
   * @see http://www.php.net/manual/en/function.PDO-quote.php
   */
  public function quoteValue(string $str): string {
    if (is_int($str) || is_float($str)) {
      return $str;
    }

    $this->pdo == null && $this->open();

    if (false === $value = $this->pdo->quote($str)) {
      // the driver doesn't support quote (e.g. oci)
      $this->close();

      return "'" . addcslashes(str_replace("'", "''", $str), "\000\n\r\\\032") . "'";
    }

    $this->close();

    return $value;
  }

  /**
   * Quotes a table name for use in a query.
   *
   * @param string $name table name
   * @return string the properly quoted table name
   */
  public function quoteTableName(string $name): string {
    return $this->quoteIdentifier($name);
  }

  /**
   * Quotes a column name for use in a query.
   * If the column name contains prefix, the prefix will also be properly quoted.
   *
   * @param string $name column name
   * @return string the properly quoted column name
   */
  public function quoteColumnName(string $name): string {
    return $this->quoteIdentifier($name);
  }

  /**
   * Quote a string that is used as an identifier (table names, column names etc).
   * This method can also deal with dot-separated identifiers eg table.column
   *
   * @param string $identifier
   * @return string
   */
  public function quoteIdentifier(string $identifier): string {
    return join('.', array_map([$this, 'quoteIdentifierPart'], explode('.', $identifier)));
  }

  /**
   * This method performs the actual quoting of a single part of an identifier,
   * using the identifier quote character.
   *
   * @param string $part
   * @return string
   */
  public function quoteIdentifierPart(string $part): string {
    return $part === '*' ? $part : "{$this->quote_character}{$part}{$this->quote_character}";
  }

  /**
   * Determines the PDO type for the specified PHP type.
   *
   * @param string $type The PHP type (obtained by gettype() call).
   *   gettype($var) returns:
   *   Possible values for the returned string are:
   *     "boolean"
   *     "integer"
   *     "double" (for historical reasons "double" is returned in case of a float, and not simply "float")
   *     "string"
   *     "array"
   *     "object"
   *     "resource"
   *     "null"
   *     "unknown type"
   *
   * @return int the corresponding PDO type
   */
  public function getPdoType(string $type): int {
    static $map = [
      'boolean' => PDO::PARAM_BOOL,
      'bool' => PDO::PARAM_BOOL,
      'integer' => PDO::PARAM_INT,
      'int' => PDO::PARAM_INT,
      'resource' => PDO::PARAM_LOB,
      'null' => PDO::PARAM_NULL,
    ];

    return $map[$type] ?? PDO::PARAM_STR;
  }

  /**
   *
   * @param string $table_name
   * @return string
   */
  public function getPrefixedTableName(string $table_name): string {
    return str_replace($this->table_prefix_placeholder, $this->table_prefix, $table_name);
  }

  /**
   *
   * @return PDO|null
   */
  public function getPDO(): ?PDO {
    return $this->pdo;
  }

  /**
   *
   * @return string
   */
  public function getHost(): string {
    return $this->host;
  }

  /**
   *
   * @return int
   */
  public function getPort(): int {
    return $this->port;
  }

  /**
   *
   * @return string
   */
  public function getDatabaseName(): string {
    return $this->db_name;
  }

  /**
   *
   * @return string
   */
  public function getUsername(): string {
    return $this->username;
  }

  /**
   *
   * @return string
   */
  public function getPassword(): string {
    return $this->password;
  }

  /**
   *
   * @return string
   */
  public function getTablePrefixPlaceholder(): string {
    return $this->table_prefix_placeholder;
  }

  /**
   *
   * @return string
   */
  public function getTablePrefix(): string {
    return $this->table_prefix;
  }

  /**
   *
   * @return string
   */
  public function getCharset(): string {
    return $this->charset;
  }

  /**
   *
   * @return string
   */
  public function getQuoteCharacter(): string {
    return $this->quote_character;
  }

  /**
   *
   * @return array
   */
  public function getConnectionOptions(): array {
    return $this->connection_options;
  }

  /**
   *
   * @return array
   */
  public function getConnectionAttributes(): array {
    return $this->connection_attributes;
  }

  /**
   *
   * @param PDO $pdo
   * @return self
   */
  public function setPDO(PDO $pdo): self {
    $this->pdo = $pdo;
    return $this;
  }

  /**
   *
   * @param string $host
   * @return self
   */
  public function setHost(string $host): self {
    $this->host = $host;
    return $this;
  }

  /**
   *
   * @param int $port
   * @return self
   */
  public function setPort(int $port): self {
    $this->port = $port;
    return $this;
  }

  /**
   *
   * @param string $database_name
   * @return self
   */
  public function setDatabaseName(string $database_name): self {
    $this->db_name = $database_name;
    return $this;
  }

  /**
   *
   * @param string $username
   * @return self
   */
  public function setUsername(string $username): self {
    $this->username = $username;
    return $this;
  }

  /**
   *
   * @param string $password
   * @return self
   */
  public function setPassword(string $password): self {
    $this->password = $password;
    return $this;
  }

  /**
   *
   * @param string $table_prefix_placeholder
   * @return self
   */
  public function setTablePrefixPlaceholder(string $table_prefix_placeholder): self {
    $this->table_prefix_placeholder = $table_prefix_placeholder;
    return $this;
  }

  /**
   *
   * @param string $table_prefix
   * @return self
   */
  public function setTablePrefix(string $table_prefix): self {
    $this->table_prefix = $table_prefix;
    return $this;
  }

  /**
   *
   * @param string $charset
   * @return self
   */
  public function setCharset(string $charset): self {
    $this->charset = $charset;
    return $this;
  }

  /**
   *
   * @param string $quote_character
   * @return self
   */
  public function setQuoteCharacter(string $quote_character): self {
    $this->quote_character = $quote_character;
    return $this;
  }

  /**
   *
   * @param array $connection_options
   * @return self
   */
  public function setConnectionOptions(array $connection_options): self {
    $this->connection_options = $connection_options;
    return $this;
  }

  /**
   *
   * @param array $connection_attributes
   * @return self
   */
  public function setConnectionAttributes(array $connection_attributes): self {
    $this->connection_attributes = $connection_attributes;
    return $this;
  }

  /**
   *
   * @return mixed
   */
  public function jsonSerialize(): mixed {
    return (object) [
      'host' => $this->host,
      'port' => $this->port,
      'db_name' => $this->db_name,
      'username' => $this->username,
      'password' => '',
      'table_prefix_placeholder' => $this->table_prefix_placeholder,
      'table_prefix' => $this->table_prefix,
      'charset' => $this->charset,
      'quote_character' => $this->quote_character,
      'connection_options' => $this->connection_options,
      'connection_attributes' => $this->connection_attributes,
    ];
  }
}
