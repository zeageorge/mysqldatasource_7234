# Simple MySQL PDO helper

Fork for use with php version >=7.2.34

## Install

Via Composer

``` bash
$ composer require zeageorge/mysqldatasource_7234
```

## Usage

``` php
<?php

namespace zeageorge\MySQLDataSource_7234;

use PDO, PDOException, Throwable;
use zeageorge\MySQLDataSource_7234\{
  Query,
  MySQLDataSource,
  MySQLDataSourceException
};

require 'vendor/autoload.php';

class Test {
};

try {
  $protected_config = require "../protected_config.php";

  $db = (new MySQLDataSource())
    ->setHost('zeageorge-dev.gr')
    ->setPort(3306)
    ->setDatabaseName('test')
    ->setUsername($protected_config->db->username)
    ->setPassword($protected_config->db->password)
    ->setTablePrefix('h4pe9_');

  $sqlq = "SELECT * FROM #__test_table WHERE id=:id1;";
  $bind_params = [':id1' => 1];

  $query = new Query([
    'query' => $sqlq,
    'bind_params' => $bind_params,
    'fetch_style' => PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE,
    'fetch_argument' => Test::class
  ]);
  // for default fetch_style just use $query = new Query("SELECT * FROM #__test_table WHERE id=:id", [':id' => 1]);

  /** @var Test[] */
  $records = $db->open()->execQuery($query);
  $db->close();
  var_dump($records);
  echo "\n";

  /** @var string */
  $new_id = $db->open()->insert('#__test_table', ['name' => 'George']);
  $db->close();
  echo "{$new_id}\n";

  /** @var string */
  $first_insert_id = $db->open()->insertMultiple('#__test_table', ['#__test_table.name'], [
    ['Gianna'],
    ['Maria'],
    ['Rena'],
    ['Toula'],
  ]);
  $db->close();
  echo "{$first_insert_id}\n";

  /** @var int */
  $affected_rows = $db->open()->update('#__test_table', ['name' => '|__|'], 'id>:id', [':id' => 10]);
  $db->close();
  echo "{$affected_rows}\n";

  /** @var int */
  $affected_rows = $db->open()->delete('#__test_table', 'id>:id', [':id' => 10]);
  $db->close();
  echo "{$affected_rows}\n";

  /** @var object[] */
  $records = $db->open()->execQuery(new Query(['query' => "SELECT * FROM #__test_table;"]));
  $db->close();
  var_dump($records);
  echo "\n";
  
} catch (MySQLDataSourceException | PDOException | Throwable $e) {
  echo get_class($e) . "\n";
  echo "{$e->getMessage()}\n";
  echo $e->getTraceAsString();
}



```

